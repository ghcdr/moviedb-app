import React, { useReducer, useContext, useState, useEffect } from 'react';
import { reducer } from './reducer';
import { fetchSome } from './fetch';

export const GlobalState = React.createContext();

const state0 = {
    storedGenres: new Set([]), // list of checked movie genres
    searchTerm: '',
};

export const GlobalStateProvider = ({ children }) => {
    const [state, dispatch] = useReducer(reducer, state0);
    const storeCheckedGenres = (genres) => {
        dispatch({ type: 'STORE_CHECKED_GENRES', payload: genres });
    };
    const storeSearchTerm = (str) => {
        dispatch({ type: 'SET_SEARCH_TERM', payload: str });
    };
    // Fetch available genres
    const [waitingConfig, setWaitingConfig] = useState(true);
    const [waitingGenresList, setWaitingGenresList] = useState(true);
    const [genresList, setGenresList] = useState([]);
    const [API_Config, setAPI_Config] = useState();
    const preFetch = async () => {
        try {
            setWaitingGenresList(true);
            setWaitingConfig(true);
            const [configuration_data, genres_data] = await Promise.all([
                fetchSome('configuration'),
                fetchSome('genre/movie/list'),
            ]);
            setAPI_Config(configuration_data['images']);
            setGenresList(genres_data['genres']);
        } catch (err) {
            console.log(err);
        }
        setWaitingGenresList(false);
        setWaitingConfig(false);
    };
    useEffect(() => {
        preFetch();
    }, []);

    return (
        <GlobalState.Provider
            value={{
                ...state,
                config: {
                    waitingConfig,
                    API_Config,
                },
                genres: {
                    waitingGenresList,
                    genresList,
                },
                storeCheckedGenres,
                storeSearchTerm,
            }}
        >
            {children}
        </GlobalState.Provider>
    );
};

export const useGlobalState = () => {
    return useContext(GlobalState);
};
