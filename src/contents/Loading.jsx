export const Loading = () => {
    return (
        <section className="loading">
            <div className="spin"></div>
        </section>
    );
};
