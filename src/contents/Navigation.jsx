import { Link, useLocation } from 'react-router-dom';
import { matchPath } from 'react-router';
import { useEffect } from 'react';

export const Navigation = () => {
    const location = useLocation();

    useEffect(() => {
        // trigger navbar re-render, to change button highlight
    }, [location]);

    const selected = (p) => {
        return matchPath(p, {
            path: window.location.pathname,
            exact: true,
            strict: true,
        });
    };
    return (
        <nav id="navigation" key="navigation" className="navigation">
            <Link
                key="home"
                className={selected('/home') ? 'nav-item-selected' : 'nav-item'}
                to="/home"
            >
                <h1>Home</h1>
            </Link>
            <Link
                key="discover"
                className={
                    selected('/discover') ? 'nav-item-selected' : 'nav-item'
                }
                to="/discover"
            >
                <h1>Discover</h1>
            </Link>
            <Link
                key="search"
                className={
                    selected('/search') ? 'nav-item-selected' : 'nav-item'
                }
                to="/search"
            >
                <h1>Search</h1>
            </Link>
        </nav>
    );
};

window.onscroll = () => scroll_callback();

const getRGB = (str) => {
    const isNum = /[0-9.]/;
    let l = '',
        rgb = [];
    for (let i = 0; i < str.length; ++i) {
        while (isNum.test(str[i]) && i < str.length) {
            l += str[i];
            ++i;
        }
        if (l.length > 0) {
            rgb.push(Number(l));
            l = '';
        }
    }
    if (rgb.length > 3) rgb.pop();
    return rgb;
};

const scroll_callback = () => {
    const lment = document.getElementById('navigation');
    const color = window.getComputedStyle(lment).backgroundColor;
    const rgb = getRGB(color);
    if (document.documentElement.scrollTop > 100) {
        lment.style.setProperty(
            'background-color',
            'rgba(' + rgb.join(', ') + ', 0.5)'
        );
    } else if (document.documentElement.scrollTop < 100) {
        lment.style.setProperty(
            'background-color',
            'rgba(' + rgb.join(', ') + ', 1.0)'
        );
    }
};
