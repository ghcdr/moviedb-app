import { useState, useEffect } from 'react';
import { MovieList } from './MovieList';
import { fetchSome } from '../fetch';
import { Loading } from './Loading';

export const Popular = () => {
    document.title = 'Popular';
    const [waiting, setWaiting] = useState(true);
    const [moviesList, setMoviesList] = useState([]);
    const fetchMovies = async () => {
        try {
            setWaiting(true);
            const movies = await fetchSome('movie/popular');
            setMoviesList(movies);
        } catch (err) {
            console.log(err);
        }
        setWaiting(false);
    };
    useEffect(() => {
        fetchMovies();
    }, []);
    return waiting ? <Loading /> : <MovieList movies={moviesList['results']} />;
};
